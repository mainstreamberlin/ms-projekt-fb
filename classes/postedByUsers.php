<?php
/**
 *
 */
class MSFbPostedByUsers
{
  public $uid;

  function __construct()
  {
    $this->uid = $_REQUEST["uid"];
  }
  /**
 	 * Block comment
 	 *
 	 * @param type
 	 * @return void
	 */
	public function sql()
  {
    $sql = "SELECT * FROM " . DB_POSTS;

    if( $this->uid ){
    $sql .= " WHERE fromuid='".$this->uid."'";
    }

    if ( ! empty( $_REQUEST['orderby'] ) ) {
      $sql .= ' ORDER BY ' . esc_sql( $_REQUEST['orderby'] );
      $sql .= ! empty( $_REQUEST['order'] ) ? ' ' . esc_sql( $_REQUEST['order'] ) : ' ASC';
    } else {
      $sql .= ' ORDER BY datum desc ';
    }

    $sql .= " LIMIT " . $this->per_page;
    $sql .= ' OFFSET ' . ( $this->page_number - 1 ) * $this->per_page;

    return $sql;
  }
  /**
 	 * Block comment
 	 *
 	 * @param type
 	 * @return void
	 */
	public function apiUsers()
  {
    global $wpdb;
    $MSFbPostedByUsers = new MSFbPostedByUsers;
    $sql = $MSFbPostedByUsers->sql();
    $sql  = substr($MSFbPostedByUsers->sql(), 0, strpos($MSFbPostedByUsers->sql(), 'LIMIT')-1);

    $users = $wpdb->get_results( $sql );
    wp_send_json($users);
    wp_die();
  }
}

?>
