<?php
/**
 *
 */
 class MSFbPages
 {

   public function __construct()
   {

   }
   /*
     fn
   */
    public function page_actions_users()
    {
      $MSFbUsertable = new MSFbUsertable;
      $MSFbUsertable->prepare_items();
      ?>
      <div class="wrap msfacebook">
          <h2 class="page-title"><span class="dashicons <?php echo $this->dashicon ?>"></span> <?php echo $GLOBALS['title'] ?></h2>
          <?php
          if( isset($_GET['action']) && $_GET['action'] == "edit" ){
            $MSFbEvents = new MSFbEvents;
            $MSFbManagedUsers = new MSFbManagedUsers;

            $args = array(
              "postbox_class" => array("first" => "col-3 first fix-inside", "second" => "col-9 last")
            );

            $widgets = array(
              array("align" => "first", "title" => "Users", "data" => $MSFbUsertable->singleDisplay() ),
              array("align" => "second", "title" => "Events", "id" => "eventlist", "height" => 70, "class" => "elli", "data" => $MSFbEvents->userSingleEvents()),
              array("align" => "second", "title" => "verwaltete Accounts", "height" => 30, "id" => "event-managed-userlist","class" => "elli","data" => $MSFbManagedUsers->ourFriends())
            );

            $MSDashboard = new MSDashboard( $widgets, $args );
            echo $MSDashboard->display();

          } else {
            ?>
            <form method="get">
        		<input type="hidden" name="page" value="<?php echo $_REQUEST['page'] ?>" />
            <?php $MSFbUsertable->search_box('search', 'search_id'); ?>
        		<?php $MSFbUsertable->display() ?>
        		</form>
          <?php
          } ?>
      </div>
      <?php
    }
    /**
 	 * Block comment
 	 *
 	 * @param type
 	 * @return void
	 */

    public function page_actions_managedUsers()
    {
      $MSFbManagedUsers = new MSFbManagedUsers; ?>
      <div class="wrap msfacebook">
        <h2 class="page-title"><span class="dashicons <?php echo $this->dashicon ?>"></span> <?php echo $GLOBALS['title'] ?></h2>
        <?php
        $args = array(
          "postbox_class" => array("first" => "col-3 first fix-inside", "second" => "col-9 last scroll-x fix-inside")
        );

        $widgets = array(
          array("align" => "first", "title" => "Users", "data" => $MSFbManagedUsers->userlist() ),
          array("align" => "second", "title" => "Freunden (".$MSFbManagedUsers->record_count().")", "data" => $MSFbManagedUsers->display())
        );

        $MSDashboard = new MSDashboard( $widgets, $args );
        echo $MSDashboard->display(); ?>

      </div>
    <?php
    }
    /**
 	 * Block comment
 	 *
 	 * @param type
 	 * @return void
	 */

    public function page_actions_foreignAccounts()
    {
      $MSFbForeignAccounts = new MSFbForeignAccounts;
      ?>
      <div class="wrap msfacebook">
        <h2 class="page-title"><span class="dashicons <?php echo $this->dashicon ?>"></span> <?php echo $GLOBALS['title'] ?></h2>
        <?php
        $args = array(
          "postbox_class" => array("first" => "col-12 fix-inside")
        );

        $widgets = array(
          array("align" => "first", "title" => "Users", "data" => $MSFbForeignAccounts->display() ),
        );

        $MSDashboard = new MSDashboard( $widgets, $args );
        echo $MSDashboard->display(); ?>
      </div>
      <?php
    }
    /**
 	 * Block comment
 	 *
 	 * @param type
 	 * @return void
	 */

    public function page_actions_events()
    {
      $FbGeneralClasses = new FbGeneralClasses;
      $MSFbEvents = new MSFbEvents;
    ?>
      <div class="wrap msfacebook">
        <h2 class="page-title"><span class="dashicons <?php echo $this->dashicon ?>"></span> <?php echo $GLOBALS['title'] ?></h2>
        <?php
        $args = array(
          "postbox_class" => array("first" => "col-3 first fix-inside", "second" => "col-9 last scroll-x fix-inside")
        );

        $widgets = array(
          array("align" => "first", "height" => 50, "class"  => "","title"          => "verwaltete Accounts", "data"      => $FbGeneralClasses->managedAccounts() ),
          // array("align" => "second","height" => 100, "class" => "elli", "title"     => "Users mit Events-Zusagen", "data" => $MSFbEvents->display()),
          array(
            "align"  => "second",
            "id"     => "box-main",
            "class"  => "elli",
            "title"  => "Users mit Events-Zusagen",
            "height" => 100,
            "ajax"   => TRUE,
            "params" => array("action" => "MSFbEvents_api", "class" => "MSFbEvents", "func" => "api"),
            "cols"   => array("fbpic", "username", "wohnort", "aus", "gender", "geburtsdatum", "age", "anz"),
            "table"  => array("searching" => 1, "paging" => 0)
          ),
          array(
            "align"  => "first",
            "class"  => "elli",
            "height" => 50,
            "ajax"   => TRUE,
            "params" => array("action" => "MSFbEvents_allEvents", "class" => "MSFbEvents", "func" => "allEvents"),
            "cols"   => array("eventname", "cat", "anzahl", "actions"),
            'rowactions' => array(
                              array(
                              'icon'        => "arrow-right-alt2",
                              'clickAction' => array("action" => "MSFbEvents_eventUsers", "class" => "MSFbEvents", "func" => "eventUsers"),
                              'filter'      => array("eventfbid", "cat"),
                              'clickTarget' => '#box-main'
                              )
                            ),
            "title"      => "Events"
          ),
        );

        $MSDashboard = new MSDashboard( $widgets, $args );
        echo $MSDashboard->display(); ?>
      </div>
    <?php
    }
  /**
 	 * Block comment
 	 *
 	 * @param type
 	 * @return void
	 */
	public function page_actions_geo()
  {
    $FbGeneralClasses = new FbGeneralClasses;
    $args     = array("country" => "DE");
    $page     = '';
    $title    = '';
    $MSFbGeo  = new MSFbGeo($args);
    foreach( $_GET as $getK => $getV ){
      $page  .= $getK . "=" .$getV ."&";
    }
    if( $MSFbGeo->uid ) $title = ', die nicht mit ' . $MSFbGeo->uid . ' befreundet sind.';
    ?>
    <div class="wrap msfacebook">
      <h2 class="page-title"><span class="dashicons <?php echo $this->dashicon ?>"></span> <?php echo $GLOBALS['title'] ?></h2>
      <?php
      $args = array(
        "postbox_class" => array("first" => "col-3 first", "second" => "col-9 last scroll-x fix-inside")
      );

      $widgets = array(
        array("align" => "first",  "title" => "Bundesland", "id" => "region", "height" => 50, "data" => $MSFbGeo->states() ),
        array("align" => "first",  "title" => "verwaltete Accounts", "height" => 50, "id" => "managed-userlist", "data" => $FbGeneralClasses->managedAccounts( $page ) ),
        array("align" => "second", "title" => "Users aus " . $MSFbGeo->bundesland . $title, "data" => $MSFbGeo->display())
      );

      $MSDashboard = new MSDashboard( $widgets, $args );
      echo $MSDashboard->display(); ?>
    </div>
    <?php
  }
  /**
 	 * Block comment
 	 *
 	 * @param type
 	 * @return void
	 */
	public function page_actions_friendsRequest()
  {
  ?>
  <div class="wrap msfacebook">
    <h2 class="page-title"><span class="dashicons <?php echo $this->dashicon ?>"></span> <?php echo $GLOBALS['title'] ?></h2>
  </div>
  <?php
  }
  /**
 	 * Block comment
 	 *
 	 * @param type
 	 * @return void
	 */
	public function page_actions_groups()
  {
    ?>
    <div class="wrap msfacebook">
      <h2 class="page-title"><span class="dashicons <?php echo $this->dashicon ?>"></span> <?php echo $GLOBALS['title'] ?></h2>
    </div>
    <?php
  }
  public function page_actions_options()
  {
    $MSFbOptions = new MSFbOptions;
    ?>
    <div class="wrap msfacebook">
      <h2 class="page-title"><span class="dashicons <?php echo $this->dashicon ?>"></span> <?php echo $GLOBALS['title'] ?></h2>
      <?php
      $args = array(
        "postbox_class" => array("first" => "col-12")
      );
      $button[0] = array("title" => "Neuer Wert", "href" => "?page=" . $_REQUEST["page"] ."&view=addnew", "id" => "");
      $widgets = array(
        array("align" => "first", "button" => $button, "title" => "Options auf der Facebook", "data" => $MSFbOptions->display() ),
      );

      $MSDashboard = new MSDashboard( $widgets, $args );
      echo $MSDashboard->display(); ?>
    </div>
    <?php
  }
 }
?>
